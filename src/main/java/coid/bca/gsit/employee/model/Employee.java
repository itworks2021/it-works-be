package coid.bca.gsit.employee.model;

import lombok.Data;

import javax.persistence.*;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "employee")
@Data
@ApiModel(value = "Employee Detail", description = "Employee Detail")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "Employee ID")
    private long id;

    @Column
    @ApiModelProperty(value = "Employee Name")
    private String name;

    @Column
    @ApiModelProperty(value = "Employee Email")
    private String email;
}
