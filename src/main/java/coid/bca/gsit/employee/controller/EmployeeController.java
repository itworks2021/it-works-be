package coid.bca.gsit.employee.controller;

import coid.bca.gsit.employee.model.Employee;
import coid.bca.gsit.employee.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@Api(tags = "Employee", value = "SErvice for Manage Employee")
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService service;

	@ApiOperation(value = "Inquiry All Employee")
	@GetMapping
	public ResponseEntity<?> getAllEmployees() {
		List<Employee> employees = service.getAllEmployees();

		if (employees.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}
		return ResponseEntity.status(HttpStatus.OK).body(employees);
	}

	@ApiOperation(value = "Inquiry Employee By ID")
	@GetMapping("/{id}")
	public ResponseEntity<?> getEmployee(@PathVariable("id") long id) {
		Optional<Employee> employee = service.getEmployeeById(id);

		if (!employee.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.status(HttpStatus.OK).body(employee.get());
	}

	@ApiOperation(value = "Add Employee")
	@PostMapping
	public ResponseEntity<?> createEmployee(@RequestBody Employee employee) {
		try {
			service.createEmployee(employee);
			return ResponseEntity.status(HttpStatus.CREATED).body(employee);
		} catch (EntityExistsException e) {
			Map<String, String> error = new HashMap<>();
			error.put("message", e.getMessage());
			return ResponseEntity.badRequest().body(error);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@ApiOperation(value = "Update Employee")
	@PutMapping("/{id}")
	public ResponseEntity<?> updateEmployee(@PathVariable("id") long id, @RequestBody Employee updatedEmployee) {
		try {
			updatedEmployee.setId(id);
			service.updateEmployee(updatedEmployee);
			return ResponseEntity.ok().body(updatedEmployee);
		} catch (EntityNotFoundException e) {
			Map<String, String> error = new HashMap<>();
			error.put("message", e.getMessage());
			return ResponseEntity.badRequest().body(error);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@ApiOperation(value = "Delete Employee")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable("id") long id) {
		try {
			service.deleteEmployee(id);
			return ResponseEntity.noContent().build();
		} catch (EntityNotFoundException e) {
			Map<String, String> error = new HashMap<>();
			error.put("message", e.getMessage());
			return ResponseEntity.badRequest().body(error);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
}
